﻿using System.Security.Cryptography;

namespace OptimizeMethod
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            byte[] salt = new byte[] { 50, 111, 8, 53, 86, 35, 19, 47, 1, 23, 54, 55, 76, 54, 34, 45 };
            GeneratePasswordHashUsingSalt("password123", salt);        
        }

        public static string GeneratePasswordHashUsingSalt(string passwordText, byte[] salt)
        {

            const int iterate = 10000;

            using var pbkdf2 = new Rfc2898DeriveBytes(passwordText, salt, iterate);

            byte[] hash = pbkdf2.GetBytes(20);

            byte[] hashBytes = new byte[36];
            Buffer.BlockCopy(salt, 0, hashBytes, 0, 16);
            Buffer.BlockCopy(hash, 0, hashBytes, 16, 20);

            return Convert.ToBase64String(hashBytes, 0, hashBytes.Length);

            
        }
    }
}